-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 20 Des 2020 pada 08.03
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ccc`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `idadmin` int(20) NOT NULL,
  `namaadmin` varchar(50) NOT NULL,
  `katasandi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`idadmin`, `namaadmin`, `katasandi`) VALUES
(2, 'Aghni', 'c0f426ac6a5d2d16ea5b4bbc33f3e8a3'),
(3, 'Puspita', '60846674a0237e7e8c4ddab9c4fbcbe5'),
(4, 'Mutmainnah', '8f5778d1dc12fae584934d1fe11b3a47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id_pelanggan` int(20) NOT NULL,
  `namapengguna` varchar(100) NOT NULL,
  `no_tlp` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `katasandi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pelanggan`
--

INSERT INTO `pelanggan` (`id_pelanggan`, `namapengguna`, `no_tlp`, `alamat`, `email`, `katasandi`) VALUES
(13, 'Medina', '087789583569', 'Jl. Betawi Bintaro', 'medina@gmail.com', '97b4e2654601727f4475bb655ffcc848'),
(14, 'Mecca', '085772386898', 'bintaro sekbil', 'mecca@gmail.com', '688376eba531b6b52c855ed22ac11ff5');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pesanan`
--

CREATE TABLE `pesanan` (
  `id_pesanan` int(100) NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `paketLaundry` varchar(100) NOT NULL,
  `berat` int(100) NOT NULL,
  `tanggalMasuk` date NOT NULL,
  `tanggalKeluar` date NOT NULL,
  `pengiriman` varchar(100) NOT NULL,
  `harga` int(100) NOT NULL,
  `pembayaran` varchar(100) NOT NULL,
  `catatan` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pesanan`
--

INSERT INTO `pesanan` (`id_pesanan`, `id_pelanggan`, `paketLaundry`, `berat`, `tanggalMasuk`, `tanggalKeluar`, `pengiriman`, `harga`, `pembayaran`, `catatan`, `status`) VALUES
(7, 13, 'Reguler (3 hari)', 6, '2020-12-15', '2020-12-18', 'Pakai jasa antar-jemput (Biaya Rp.10.000)', 70000, 'BCA | 6439864864 (NADA)', '---', 'Selesai'),
(8, 14, 'Super Express (1 hari)', 1, '2020-12-18', '2020-12-19', 'Pakai jasa antar-jemput (Biaya Rp.10.000)', 20000, 'COD (Bayar Di Tempat)', '---', 'Selesai'),
(9, 14, 'Reguler (3 hari)', 4, '2020-12-16', '2020-12-19', 'Antar Sendiri', 40000, 'BRI | 1168497492 (MENTARI)', '--', 'Proses Pengantaran'),
(12, 13, 'Express (2 hari)', 5, '2020-12-19', '2020-12-21', 'Antar Sendiri', 50000, 'C0D(Bayar Di Tempat)', '---', 'Diterima pegawai');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`idadmin`);

--
-- Indeks untuk tabel `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indeks untuk tabel `pesanan`
--
ALTER TABLE `pesanan`
  ADD PRIMARY KEY (`id_pesanan`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `idadmin` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id_pelanggan` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `pesanan`
--
ALTER TABLE `pesanan`
  MODIFY `id_pesanan` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
